<?php

namespace app\controllers;

use yii\web\Response;
use app\models\Tarefas;

class TarefasController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $model = new Tarefas();
        \Yii::$app->response->format = Response::FORMAT_JSON;
        return $model::find()->all();
    }


    public function actionCreate($tarefa)
    {

        $model = new Tarefas();
        $model->title = $tarefa;
        

        \Yii::$app->response->format = Response::FORMAT_JSON;
        return $model->save();

    }



    public function actionEdit($id,$tarefa)
    {

        $model = new Tarefas();
        $model  = $model->findOne($id);
        $model->title = $tarefa;
        $model->save();

        \Yii::$app->response->format = Response::FORMAT_JSON;
        return true;

    }

    public function actionDelete($id)
    {

        $model = new Tarefas();
        

        \Yii::$app->response->format = Response::FORMAT_JSON;
        return $model->findOne($id)->delete();
        

    }
}
