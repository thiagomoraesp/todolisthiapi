const HtmlWebPackPlugin = require("html-webpack-plugin");

module.exports = function (api) {
    api.cache(true);
  
    const presets = ['@babel/preset-react',"mobx"];
    const plugins =[
             ["@babel/plugin-proposal-decorators", { "legacy": true}],
            ["@babel/plugin-proposal-class-properties", { "loose": true}]
    ]
  
    return {
      presets,
      plugins
    };
  }