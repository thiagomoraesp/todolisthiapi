import React, { Component } from 'react';
import './App.css';
import { CampoTexto,Salvar,Contador} from "./inputs.js";
import { ListaDeTarefas} from "./ListaDeTarefas";
import "antd/dist/antd.css";
import { observable,toJS } from 'mobx';
import {observer} from "mobx-react";


class Estado  {
 
  @observable
  count=0;

  @observable
  ListaDeItens=[];

  @observable
  rascunho_edicao="";

  @observable
  rascunho_novo = "";
  

  obtemLista()
  {
    return fetch('/api/tarefas/index')
    .then(response => response.json())
    .then(data => this.ListaDeItens.replace(data),
    );
    
  }

  atualizaRascunhoNovo(e)
  {   
    this.rascunho_novo = e.target.value;
  }

  salvaEdicao(e,item)
  {  
    
    fetch("api/tarefas/edit?id="+item['id']+'&tarefa='+this.rascunho_edicao)
    .then((response) => this.obtemLista());
     
  }
  
  salvaNovo(e)
  {
    e.preventDefault();
    fetch("/api/tarefas/create?tarefa="+this.rascunho_novo)
    .then((response) => this.obtemLista())
  }


    excluir(elemento)
  {

    
    fetch("/api/tarefas/delete?id="+elemento['id'])
    .then((response) => this.obtemLista())  
  }
  
  inicia_rascunho(e,item)
  {

    if(e===false)
    {
      //this.salvaItem(e,item);
    }
    else
    {
      this.rascunho_edicao = item;
      console.log(this.rascunho_edicao);
    }
    
  }

  onChangeEdicao(evento)
  {
    

     this.rascunho_edicao = evento.target.value;
  }



  
  incrementa(){
    this.count++;
  };
  decrementa(){
    this.count--;
  };

};





@observer
class FormularioAdd extends Component{



  render() {
    return(
      <form onSubmit={this.props.acao}>
      <CampoTexto estado = {this.props.estado} nome="NovaTarefa" />
      <Salvar estado = {this.props.estado} nome="salvarNovaTarefa" />

      </form>

    );


  }
}




class App extends Component {

  constructor(props){
    super(props)
  }

  componentWillMount()
  {
    this.contador = new Estado();
    this.estado = new Estado();
  }

  componentDidMount()
    {
     
      this.estado.obtemLista();


    }

  render() {
    
    
    
    return (
      <div className="App">

        

        <FormularioAdd estado = {this.estado}/>
        <ListaDeTarefas  estado={this.estado}/>
        <Contador atualiza_rascunho={(e) =>this.estado.atualizaRascunhoNovo(e)} contador={this.contador}  />
      </div>
    );
  }
}

export default App;
