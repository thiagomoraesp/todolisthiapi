import React, { Component } from 'react';
import './App.css';
import "antd/dist/antd.css";
import {Input,Button,Divider} from "antd";
import {observer} from "mobx-react"

@observer
class EdicaoTexto extends Component{

    //
      
    
      render() {
    
    console.log(this.props.item.title);
        return(
    
          <div >
          <Input onChange={(e)=>this.props.estado.onChangeEdicao(e)} onBlur={(e,item) => this.props.estado.salvaEdicao(e,this.props.item)} value={this.props.estado.rascunho_edicao} type="text" />
          
          </div>
      );
      }
    }

@observer    
class CampoTexto extends Component{   
      render() {
        return(
    
          <input onChange ={(e)=>this.props.estado.atualizaRascunhoNovo(e)} value={this.props.estado.rascunho_novo} />
      );
      }
    }
    
class Salvar extends Component{



        render() {
          return(
            <button onClick={(e)=>this.props.estado.salvaNovo(e)} name={this.props.nome} value="Salvar">Salvar</button>
      
        );
        }
      }

@observer
class Contador extends Component{


    

        render() {
          
          
          let contador = this.props.contador;

          return(
            
            <div>
            <button onClick={()=>contador.incrementa()}>+</button>
            <div>contagem: {contador.count}</div>
            <button onClick={()=>contador.decrementa()}>-</button>
            </div>
      
        )
        }
      }
      

export{EdicaoTexto,CampoTexto,Salvar,Contador}      