import React, { Component } from 'react';
import './App.css';
import "antd/dist/antd.css";
import { Table, Divider,Popover,Button,Input} from "antd";
import { EdicaoTexto} from "./inputs.js";
import {observer} from 'mobx-react';
import {toJS} from 'mobx';



@observer
class ListaDeTarefas extends Component{


  constructor(props){
    super(props)
    
  }




    render(){
    
   //id,title

//console.log(toJS(this.props.tarefas));

const columns = [{
  title: 'id',
  dataIndex: 'id',
  key: 'id',
}, {
  title: 'titulo',
  dataIndex: 'title',
  key: 'title',
},
{
  title: 'Ações',
  key: 'action',
  render: (text, record) => (
    <span>
      {/*FUNÇÃO ANONIMA, LEMBRAR, SÓ VOU SUAR SE QUISER PEGAR ALGO DO COMPONENTE QUE ESTÁ CHAMANDO ELA*/}
      <a onClick={()=>this.props.estado.excluir(record)} href="#">excluir</a>
      <Divider type="vertical" />
      <Popover
        content={<EdicaoTexto estado={this.props.estado}item={record}/>}
        title={"Editar " +record.title}
        trigger="click"
        onVisibleChange={(e)=>this.props.estado.inicia_rascunho(e,record.title)}
        
      >
        <Button type="primary">editar</Button>
      </Popover>
    </span>
  ),
}

];

      let lista = toJS(this.props.estado.ListaDeItens);
      
      return(
      <div className="Lista">



        <Table dataSource={lista} columns={columns}/>

        </div>
    );


  }
}


export {ListaDeTarefas};