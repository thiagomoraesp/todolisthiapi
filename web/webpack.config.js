const path = require('path');
//const HtmlWebPackPlugin = require("html-webpack-plugin");
module.exports = {
  stats: 'errors-only',
  entry: './src/index.js',
  output: {
    filename: 'main.js',
    path: path.resolve(__dirname, 'dist')
  },
  mode:'development',
  optimization: {
		// We no not want to minimize our code.
		minimize: false
	},
  module:{
    
    rules: [
      {
        test: /\.m?js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader',
          /*options: {
            presets: ['@babel/preset-env', '@babel/preset-react'],
            plugins:[ new HtmlWebPackPlugin({
              template: "./src/index.html",
              filename: "./index.html"
            })]
          }*/
        }
        
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader'],
      },
    ]
  

  }


};
