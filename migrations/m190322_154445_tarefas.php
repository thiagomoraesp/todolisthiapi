<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Class m190322_154445_tarefas
 */
class m190322_154445_tarefas extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {


      $this->createTable('tarefas', [
          'id' => Schema::TYPE_PK,
          'title' => Schema::TYPE_STRING . ' NOT NULL',
          /*'content' => Schema::TYPE_TEXT,*/
      ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('tarefas');

        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190322_154445_tarefas cannot be reverted.\n";

        return false;
    }
    */
}
